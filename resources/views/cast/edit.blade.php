@extends('layout.master')

@section('judul')
Halaman Edit Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label >Nama Cast</label>
    <input type="text" name="nama" value ="{{$cast->nama}}" class="form-control">
  </div>
<!-- error handling untuk nama -->
  @error('nama')
      <div class="alert alert-danger">{{ 'Nama Cast harus diisi' }}</div>
  @enderror('nama')

  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="number" name="umur" class="form-control">
  </div>
  <!-- error handling untuk umur -->
    @error('umur')
        <div class="alert alert-danger">{{ 'Umur harus diisi' }}</div>
    @enderror


  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <textarea name="bio" class="form-control" rows="8" cols="80"></textarea>
  </div>
  <!-- error handling untuk bio -->
    @error('bio')
        <div class="alert alert-danger">{{ 'Bio harus diisi' }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection
