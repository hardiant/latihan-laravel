@extends('layout.master')

@section('judul')
  Halaman Data Tabel
@endsection

@section('content')

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form class="" action="/welcome" method="post">
  @csrf
  <label>First Name:</label> <br><br>
  <input type="text" name="nama1" value=""> <br><br>
  <label>Last Name:</label> <br><br>
  <input type="text" name="nama2" value=""><br><br>

  <label>Gender:</label> <br><br>
  <input type="radio" name="kelamin" value="male">Male <br>
  <input type="radio" name="kelamin" value="female">Female <br>
  <input type="radio" name="kelamin" value="other">Other <br><br>

  <label>Nationality:</label> <br><br>
  <select class="" name="kebangsaan">
    <option value="1">Indonesia</option>
    <option value="2">Other</option>
  </select> <br><br>

  <label>Language Spoken:</label> <br><br>
  <input type="checkbox" name="bahasa" >Indonesia<br>
  <input type="checkbox" name="bahasa" >English<br>
  <input type="checkbox" name="bahasa" >Other <br><br>

<label>Bio:</label> <br><br>
<textarea name="bio" rows="8" cols="30"></textarea><br><br>
<input type="submit" name="submit" value="Submit">

</form>

@endsection
